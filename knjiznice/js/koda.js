/* global $, d3 */

var baseUrl = 'https://rest.ehrscape.com/rest/v1';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta, callback) {
  var ehrId = "", pod = {}, meritve = [{},{},{}];
  //registriramo novega bolnika
  $.ajax({
		url: baseUrl+"/ehr",
		type: "POST",
		headers: {
			"Authorization": getAuthorization()
    },
    success: function(data) {
      ehrId = data.ehrId;
      //bolniku nastavimo osnovne podatke
      switch(stPacienta) {
        case 1:
          pod = {
            firstNames : "Jaka",
            lastNames : "Rozman",
            gender : "MALE",
            dateOfBirth: "1997-06-18",
            additionalInfo: {"ehrId": ehrId}
          };
          for(var i = 1; i <= 5; i++) {
            meritve[i-1] = {
              "ctx/language": "en",
              "ctx/territory": "SI",
              "ctx/time": "2019-05-0"+String(i)+"T15:10Z",
              "vital_signs/height_length/any_event/body_height_length": (189+Math.floor(Math.random()*6)),
              "vital_signs/body_weight/any_event/body_weight": (83.2+Math.floor(Math.random()*60)/10)
            };
          }
          break;
        case 2:
          pod = {
            firstNames : "Nika",
            lastNames : "Kobal",
            gender : "FEMALE",
            dateOfBirth: "2007-11-26",
            additionalInfo: {"ehrId": ehrId}
          };
          for(i = 1; i <= 5; i++) {
            meritve[i-1] = {
              "ctx/language": "en",
              "ctx/territory": "SI",
              "ctx/time": "2019-04-0"+String(i)+"T15:10Z",
              "vital_signs/height_length/any_event/body_height_length": (146+Math.floor(Math.random()*6)),
              "vital_signs/body_weight/any_event/body_weight": (44.9+Math.floor(Math.random()*60)/10)
            };
          }
          break;
        case 3:
          pod = {
            firstNames : "Marija",
            lastNames : "Jerman",
            gender : "FEMALE",
            dateOfBirth: "1956-04-07",
            additionalInfo: {"ehrId": ehrId}
          };
          for(i = 1; i <= 5; i++) {
            meritve[i-1] = {
              "ctx/language": "en",
              "ctx/territory": "SI",
              "ctx/time": "2019-03-0"+String(i)+"T15:10Z",
              "vital_signs/height_length/any_event/body_height_length": (168+Math.floor(Math.random()*6)),
              "vital_signs/body_weight/any_event/body_weight": (79.3+Math.floor(Math.random()*60)/10-3)
            };
          }
          break;
      }
			$.ajax({
				url: baseUrl+"/demographics/party",
				type: "POST",
				headers: {
					"Authorization": getAuthorization()
				},
				contentType: "application/json",
				data: JSON.stringify(pod),
				success: function(party) {
					if (party.action == "CREATE") {
						var opt1 = document.createElement("option"), opt2 = document.createElement("option");
						opt1.value = ehrId, opt2.value = ehrId;
						opt1.innerHTML = pod.firstNames+" "+pod.lastNames, opt2.innerHTML = pod.firstNames+" "+pod.lastNames;
						document.getElementById("bolnikI").appendChild(opt1);
						document.getElementById("bolnikM").appendChild(opt2);
						
						var parametriZahteve = {
              ehrId: ehrId,
              templateId: "Vital Signs",
              format: "FLAT",
              committer: "Zdravnik"
            };
            //bolniku dodamo meritev 1
            $.ajax({
              url: baseUrl+"/composition?"+$.param(parametriZahteve),
              type: "POST",
              contentType: "application/json",
              data: JSON.stringify(meritve[0]),
              headers: {
                "Authorization": getAuthorization()
              },
              success: function(res) {
                //bolniku dodamo meritev 2
                $.ajax({
                  url: baseUrl+"/composition?"+$.param(parametriZahteve),
                  type: "POST",
                  contentType: "application/json",
                  data: JSON.stringify(meritve[1]),
                  headers: {
                    "Authorization": getAuthorization()
                  },
                  success: function(res) {
                    //bolniku dodamo meritev 3
                    $.ajax({
                      url: baseUrl+"/composition?"+$.param(parametriZahteve),
                      type: "POST",
                      contentType: "application/json",
                      data: JSON.stringify(meritve[2]),
                      headers: {
                        "Authorization": getAuthorization()
                      },
                      success: function(res) {
                        //bolniku dodamo meritev 4
                        $.ajax({
                          url: baseUrl+"/composition?"+$.param(parametriZahteve),
                          type: "POST",
                          contentType: "application/json",
                          data: JSON.stringify(meritve[3]),
                          headers: {
                            "Authorization": getAuthorization()
                          },
                          success: function(res) {
                            //bolniku dodamo meritev 5
                            $.ajax({
                              url: baseUrl+"/composition?"+$.param(parametriZahteve),
                              type: "POST",
                              contentType: "application/json",
                              data: JSON.stringify(meritve[4]),
                              headers: {
                                "Authorization": getAuthorization()
                              },
                              success: function(res) {
                                callback(ehrId);
                              },
                              error: function(err) {
                                console.log("narobe 5");
                                console.log(err.responseText);
                              }
                            });
                          },
                          error: function(err) {
                            console.log("narobe 4");
                            console.log(err.responseText);
                          }
                        });
                      },
                      error: function(err) {
                        console.log("narobe 3");
                        console.log(err.responseText);
                      }
                    });
                  },
                  error: function(err) {
                    console.log("narobe 2");
                    console.log(err.responseText);
                  }
                });
              },
              error: function(err) {
                console.log("narobe 1");
                console.log(err.responseText);
              }
            });
          }
				},
				error: function(err) {
				  console.log(err);
				}
			});
		},
		error: function(err) {
		  console.log(err);
		}
  });
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

var podatki;

window.addEventListener("load", function() {
  
  //pridobimo podatke statistike za drzave
  var xobj = new XMLHttpRequest();
  xobj.open("GET", "knjiznice/json/stats_en.json", true);
  xobj.onreadystatechange = function() {
    if(xobj.readyState == 4 && xobj.status == "200") {
      podatki = (JSON.parse(xobj.responseText)).data;
      for(var i = 0; i < podatki.length; i++) {
        var opt = document.createElement("option");
        opt.value = i;
        opt.innerHTML = podatki[i].drzava;
        document.getElementById("drzavaI").appendChild(opt);
      }
    }
  };
  xobj.send();
  
});

//vzame vpisane parametre in ustvari nov EHR zapis
function kreirajNovega() {
  var ime = $("#izbranoIme").val(), priimek = $("#izbranPriimek").val(), spol = $("#izbranSpol").val(), datum = $("#izbranDatum").val();
  if(!ime || !priimek || !spol || !datum) {
    document.getElementById("sporociloK").innerHTML = '<span class="label label-warning">Vnesite vse zahtevane podatke!</span>';
  } else {
    $.ajax({
			url: baseUrl+"/ehr",
			type: "POST",
			headers: {
				"Authorization": getAuthorization()
			},
			success: function(data) {
				var ehrId = data.ehrId;
				var dataNew = {
					firstNames: ime,
					lastNames: priimek,
					gender: spol,
					dateOfBirth: datum,
					additionalInfo: {"ehrId": ehrId}
				};
				$.ajax({
					url: baseUrl+"/demographics/party",
					type: "POST",
					headers: {
						"Authorization": getAuthorization()
					},
					contentType: "application/json",
					data: JSON.stringify(dataNew),
					success: function(party) {
            if (party.action == "CREATE") {
              document.getElementById("sporociloK").innerHTML = '<span class="label label-success">Uspešno kreiran bolnik '+ime+' '+priimek+'!</span>'+
						                                                    '<br><span class="label label-success">('+ehrId+')</span>';
							
							var opt1 = document.createElement("option"), opt2 = document.createElement("option");
							opt1.value = ehrId, opt2.value = ehrId;
							opt1.innerHTML = ime+" "+priimek, opt2.innerHTML = ime+" "+priimek;
							document.getElementById("bolnikI").appendChild(opt1);
							document.getElementById("bolnikM").appendChild(opt2);
						}
					},
					error: function(err) {
					  document.getElementById("sporociloK").innerHTML = '<span class="label label-danger">Napaka '+err.responseText+'!</span>';
					}
				});
			},
			error: function(err) {
			  document.getElementById("sporociloK").innerHTML = '<span class="label label-danger">Napaka '+err.responseText+'!</span>';
			}
		});
  }
}

//zgenerira 3 različne uporabnike
function zgeneriraj() {
  generirajPodatke(1, function(id1) {
    generirajPodatke(2, function(id2) {
      generirajPodatke(3, function(id3) {
        alert("Zgenerirani so bili trije uporabniki:\n - "+id1+"\n - "+id2+"\n - "+id3);
      });
    });
  });
  document.getElementById("gumb").disabled = true;
}

//posodobi ehr vnose v inputih pri dodajanju meritev
function posodobiM() {
  $("#meritevEhrId").val($("#bolnikM").val());
}

//doda meritve bolniku
function dodajMeritve() {
  var ehrId = $("#meritevEhrId").val(), visina = $("#meritevVisina").val(), teza = $("#meritevTeza").val(), datum = $("#meritevDatum").val();
  if(!ehrId || !visina || !teza || !datum) {
    document.getElementById("sporociloM").innerHTML = '<span class="label label-warning">Vnesite vse zahtevane podatke!</span>';
  } else {
    var meritve = {
      "ctx/language": "en",
      "ctx/territory": "SI",
      "ctx/time": datum+"Z",
      "vital_signs/height_length/any_event/body_height_length": visina,
      "vital_signs/body_weight/any_event/body_weight": teza
    };
    var parametriZahteve = {
      ehrId: ehrId,
      templateId: "Vital Signs",
      format: "FLAT",
      committer: "Zdravnik"
    };
    $.ajax({
      url: baseUrl+"/composition?"+$.param(parametriZahteve),
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify(meritve),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function(res) {
        document.getElementById("sporociloM").innerHTML = '<span class="label label-success">Uspešno dodani vitalni znaki!</span>'+
						                                              '<br><span class="label label-success">('+ehrId+')</span>';
      },
      error: function(err) {
        document.getElementById("sporociloM").innerHTML = '<span class="label label-danger">Napaka '+err.responseText+'!</span>';
      }
    });
  }
}

//posodobi ehr vnose v inputih pri prikazu
function posodobiI() {
  $("#ehrI").val($("#bolnikI").val());
}

//prikazi grafe za izbranega bolnika
function prikaziInfo() {
  var ehrId = $("#ehrI").val(), drzava =  $("#drzavaI").val();
  if(!ehrId || !drzava) {
    document.getElementById("sporociloI").innerHTML = '<span class="label label-warning">Vnesite zahtevane podatke ali izberite bolnika!</span>';
  } else {
    document.getElementById("sporociloI").innerHTML = "";
    $.ajax({
      url: baseUrl+"/demographics/ehr/"+ehrId+"/party",
      type: "GET",
      headers: {
        "Authorization": getAuthorization()
      },
      success: function(data) {
        $.ajax({
          url: baseUrl+"/view/"+ehrId+"/height",
          type: "GET",
          headers: {
            "Authorization": getAuthorization()
          },
          success: function(resV) {
            $.ajax({
              url: baseUrl+"/view/"+ehrId+"/weight",
              type: "GET",
              headers: {
                "Authorization": getAuthorization()
              },
              success: function(resT) {
                izrisiPodatke(data.party, resV, resT, drzava);
              },
              error: function(err) {
                console.log(err);
              }
            });
          },
          error: function(err) {
            console.log(err);
          }
        });
      },
      error: function(err) {
        console.log(err);
      }
    });
  }
}

//izpise rezultate
function izrisiPodatke(user, visine, teze, drzava) {
  if(visine.length == 0 || teze.length == 0) {
    document.getElementById("sporociloI").innerHTML = '<span class="label label-warning">Bolnik nima vseh zahtevanih meritev!</span>';
    return;
  }
  document.getElementById("sporociloI").innerHTML = "";
  
  var bmi = vrniBMI(visine, teze);
  
  document.getElementById("rezultati").removeAttribute("class", "hidden");

  document.querySelector("visina").innerHTML = "";
  document.querySelector("teza").innerHTML = "";
  document.querySelector("bmi").innerHTML = "";

  document.getElementById("imePriimek").innerHTML = user.firstNames+" "+user.lastNames+" ("+((user.gender == "MALE" ? "moški" : (user.gender == "FEMALE" ? "ženska" : "neznan spol"))+"), rojen/a "+razcleniDatum(user.dateOfBirth));
  document.getElementById("opis").innerHTML = "Primerjava bolnikovih meritev s povprečjem bolnika izbrane države ("+podatki[drzava].drzava+") in prikaz bolnikovih meritev skozi čas.";
  
  document.getElementById("bolnikVisina").innerHTML = visine[0].height+" "+visine[0].unit;
  document.getElementById("drzavaImeV").innerHTML = podatki[drzava].drzava+":";
  document.getElementById("drzavaVisina").innerHTML = (user.gender == "" ? "ni podatka za bolnikov spol" : ((user.gender == "MALE" ? podatki[drzava].moski.visina*100 : podatki[drzava].zenske.visina*100)+" "+visine[0].unit));
  document.getElementById("visinaRazlika").innerHTML = (user.gender ? (visine[0].height-(user.gender == "MALE" ? podatki[drzava].moski.visina*100 : podatki[drzava].zenske.visina*100)+" "+visine[0].unit) : "ni podatka za bolnikov spol");
  
  document.getElementById("bolnikTeza").innerHTML = Math.round(teze[0].weight*10)/10+" "+teze[0].unit;
  document.getElementById("drzavaImeT").innerHTML = podatki[drzava].drzava+":";
  document.getElementById("drzavaTeza").innerHTML = (user.gender ? ((user.gender == "MALE" ? podatki[drzava].moski.teza : podatki[drzava].zenske.teza)+" "+teze[0].unit) : "ni podatka za bolnikov spol");
  document.getElementById("tezaRazlika").innerHTML = (user.gender ? (Math.round((teze[0].weight-(user.gender == "MALE" ? podatki[drzava].moski.teza : podatki[drzava].zenske.teza))*10)/10+" "+teze[0].unit) : "ni podatka za bolnikov spol");
  
  document.getElementById("bolnikBMI").innerHTML = bmi[0];
  document.getElementById("drzavaImeB").innerHTML = podatki[drzava].drzava+":";
  document.getElementById("drzavaBMI").innerHTML = (user.gender ? (user.gender == "MALE" ? podatki[drzava].moski.bmi : podatki[drzava].zenske.bmi) : "ni podatka za bolnikov spol");
  document.getElementById("bmiRazlika").innerHTML = (user.gender ? Math.round((bmi[0]-(user.gender == "MALE" ? podatki[drzava].moski.bmi : podatki[drzava].zenske.bmi))*10)/10 : "ni podatka za bolnikov spol");
  
  var margin = {top: 0, right: 50, bottom: 50, left: 50}, width = 200, height = 100;
  //za visine
  var xScaleV = d3.scaleLinear().domain([1, visine.length]).range([0, width]);
  var yScaleV = d3.scaleLinear().domain(minmaxV(visine)).range([height, 0]);
  var lineV = d3.line()
    .x(function(d, i) {return xScaleV(visine.length-i);})
    .y(function(d) {return yScaleV(d.height);})
    .curve(d3.curveMonotoneX);
  var v = d3.select("visina").append("svg")
    .attr("width", width+margin.left+margin.right)
    .attr("height", height+margin.top+margin.bottom)
    .append("g")
    .attr("transform", "translate("+margin.left+","+margin.top+")");
  v.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0,"+height+")")
    .call(d3.axisBottom(xScaleV));
  v.append("g")
    .attr("class", "y axis")
    .call(d3.axisLeft(yScaleV));
  v.append("path")
    .datum(visine)
    .attr("class", "line v")
    .attr("d", lineV);
  //za teze
  var xScaleT = d3.scaleLinear().domain([1, teze.length]).range([0, width]);
  var yScaleT = d3.scaleLinear().domain(minmaxT(teze)).range([height, 0]);
  var lineT = d3.line()
    .x(function(d, i) {return xScaleT(teze.length-i);})
    .y(function(d) {return yScaleT(d.weight);})
    .curve(d3.curveMonotoneX);
  var t = d3.select("teza").append("svg")
    .attr("width", width+margin.left+margin.right)
    .attr("height", height+margin.top+margin.bottom)
    .append("g")
    .attr("transform", "translate("+margin.left+","+margin.top+")");
  t.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0,"+height+")")
    .call(d3.axisBottom(xScaleT));
  t.append("g")
    .attr("class", "y axis")
    .call(d3.axisLeft(yScaleT));
  t.append("path")
    .datum(teze)
    .attr("class", "line t")
    .attr("d", lineT);
  //za bmi
  var xScaleB = d3.scaleLinear().domain([1, bmi.length]).range([0, width]);
  var yScaleB = d3.scaleLinear().domain(minmaxB(bmi)).range([height, 0]);
  var lineB = d3.line()
    .x(function(d, i) {return xScaleB(bmi.length-i);})
    .y(function(d) {return yScaleB(d);})
    .curve(d3.curveMonotoneX);
  var b = d3.select("bmi").append("svg")
    .attr("width", width+margin.left+margin.right)
    .attr("height", height+margin.top+margin.bottom)
    .append("g")
    .attr("transform", "translate("+margin.left+","+margin.top+")");
  b.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0,"+height+")")
    .call(d3.axisBottom(xScaleB));
  b.append("g")
    .attr("class", "y axis")
    .call(d3.axisLeft(yScaleB));
  b.append("path")
    .datum(bmi)
    .attr("class", "line f")
    .attr("d", lineB);
  dodajKomentar(bmi);
}

function dodajKomentar(bmi) {
  //uporabi linearno regresijo za izračun koeficienta
  var d = document.getElementById("predlog");
  var mtr = [[0, 0], [0, bmi.length]], b = [0, 0];
  for(var i = 0; i < bmi.length; i++) {
    mtr[0][0] += bmi[i]*bmi[i];
    mtr[0][1] += bmi[i];
    b[0] += i*bmi[i];
    b[1] += i;
  }
  mtr[1][0] = mtr[0][1];
  var k1 = mtr[0][1]/mtr[1][1];
  var k = (b[0]-k1*b[1])/(mtr[0][0]-k1*mtr[1][0]);
  k *= -1;
  if(k > 0) {
    d.innerHTML = "Vaš ITM s časom narašča za "+Math.round(k*10)/10+"%!";
  } else {
    d.innerHTML = "Vaš ITM s časom pada za "+Math.round(-k*10)/10+"%!";
  }
}

function vrniBMI(visine, teze) {
  var obj = [];
  for(var i = 0; i < visine.length; i++) {
    obj.push(Math.round(teze[i].weight/(visine[i].height*visine[i].height)*100000)/10);
  }
  return obj;
}

function razcleniDatum(date) {
  var a = date.split("-");
  return a[2]+"."+a[1]+"."+a[0];
}

function minmaxV(visine) {
  var obj = [visine[0].height, visine[0].height];
  for(var i = 1; i < visine.length; i++) {
    if(obj[0] > visine[i].height) {obj[0] = visine[i].height;}
    if(obj[1] < visine[i].height) {obj[1] = visine[i].height;}
  }
  obj[0] -= 3, obj[1] += 3;
  return obj;
}
function minmaxT(teze) {
  var obj = [teze[0].weight, teze[0].weight];
  for(var i = 1; i < teze.length; i++) {
    if(obj[0] > teze[i].weight) {obj[0] = teze[i].weight;}
    if(obj[1] < teze[i].weight) {obj[1] = teze[i].weight;}
  }
  obj[0] -= 3, obj[1] += 3;
  return obj;
}
function minmaxB(bmi) {
  var obj = [bmi[0], bmi[0]];
  for(var i = 1; i < bmi.length; i++) {
    if(obj[0] > bmi[i]) {obj[0] = bmi[i];}
    if(obj[1] < bmi[i]) {obj[1] = bmi[i];}
  }
  obj[0] -= 2, obj[1] += 2;
  return obj;
}