file = open("stats.txt", "r")
json = open("stats_en.json", "w")
json.write("{\"origin\":\"https://www.worlddata.info")
json.write("/average-bodyheight.php\",\"data\":[\n")
for i in range(129):
    vrstica = file.readline()
    start = 0
    podatki = []
    for j in range(len(vrstica)):
        if vrstica[j] is '\t':
            podatki.append(vrstica[start:j])
            start = j
    podatki.append(vrstica[start:len(vrstica)])
    print(podatki)
    json.write('{"drzava":"'+str(podatki[0])+'",')
    json.write('"moski":{"visina":'+str(podatki[2][1:podatki[2].find(' ')])+',"teza":'+str(podatki[3][1:podatki[3].find(' ')])+',"bmi":'+str(podatki[4][1:5])+'},')
    json.write('"zenske":{"visina":'+str(podatki[6][1:podatki[6].find(' ')])+',"teza":'+str(podatki[7][1:podatki[7].find(' ')])+',"bmi":'+str(podatki[8][1:5])+'}')
    json.write("}")
    if i is not 128:
        json.write(",\n")

json.write("]}")
json.close()
file.close()
